# Grafana Docker/NGINX

There are 2 parts to running grafana behind a nginx proxy server.

## NGiNX
First Setup NGiNX

Setup nginx to point to the correct ip address.

Make sure that the rewrite line exists to copy the values.

NGINX config
```
      location /grafana/ {
                proxy_pass http://192.168.0.102:3001/;
                proxy_http_version 1.1;
                rewrite  ^/grafana/(.*)  /$1 break;
                proxy_buffering off;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
                gzip off;
       }
```

## Grafana Config

Need to update the grafana.ini file to some where to map locally.

Place the ini somewhere to map as a volume, edit the ini and the docker-compose-yml to make server settings.  I.e change 100.116.229.68 to your ip or hostname.

```
[server]
root_url = https://100.116.229.68:3001/grafana/
domain = 100.116.229.68```
```

