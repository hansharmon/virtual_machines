#amqp #docker #qpid

# Broker-J - Docker Hub
This is the AMQP Docker stored up in hub.docker.com.  This is the official baseline for AMQP Broker-J
This the Java version of the broker with a web front end.

## Setup
This is pretty easy to get moving, but a couple of things need to be done to get it working completely

Run docker compose
```bash
sudo docker-compose up -d
```

This will download and install the docker from docker hub.

## Web UI Setup
Connect to the Web UI http://[ip]:8080.  Adjust the port number to match if changed in the docker-compose xml. 

The default user and password are both admin.
User:  admin
password:  admin

On the left side, there is a folder tree.  Double clicking on the folders or files will open up controls on the right side.  Some items will work others will do nothing.

This will be missing a topic and queue and one will need to be added to make this work.

### Change Admin Password
Frist thing should be to open up authtenticationproviders->plain->users->admin by double clicking on 'plain'.  Then change the password using the controls on the right, select the user admin and double click to change the password
![](AMQP%20-%20Broker%20-J%20-%20Tree%20Setup.jpg)

## Create Exchanges and Queue
Need to create exchanges so that a topic and queue can used to communicate over QPID.
First go to the virtualhostnodes->default->virtualhost->default and double click on the defaults it will bring up the following page:
![](AMQP%20-%20Broker%20-J%20-%20Creating%20Topics%20and%20Queues.jpg)

### Add Queue
Click on Add Queue and fill out the setup as follows.  Queues allow the exchang to store values for a while.
![](AMQP%20-%20Broker%20-J%20-%20Add%20Queue.jpg)

### Add Exchange
Next click on add exchange.  Setup a topic with a the name mytpoic and exchange type of topic add the myqueue as an alternative binding, this allows the topic to now store some messages before getting a rejection or lost messages.
![](AMQP%20-%20Broker%20-J%20-%20Add%20Exchange.jpg)


### Command line tools.
install ampq-tools and then can use command line to test

```bash
sudo apt install amqp-tools
```

### Consumer
```bash
amqp-consume --username=admin --password=admin -q myqueue  cat

```

### Publisher
```bash
amqp-publish -e mytopic --username=admin --password=admin -b "Testing"
```

