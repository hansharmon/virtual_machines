# Docker Setup
This file will install docker on a linux machine that does not have docker.

## Requirements
Assumes that apt is used.  Change to yum or opkg or whatever.

## Usage
 ```bash
 ./setup.sh
 ```

## How it works
- Updates the package feed
- Installs curls and ca-certificats
    - Curl for web downloading
    - Ca-certificates to install docker certs.
- Downloads and installs Docker certs
- Downloads and installs Docker

## See also
Recommend installing portainer next.

