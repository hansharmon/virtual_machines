# nginx.conf
# This config sends http(s) requests to gunicorn/flask application.
user flaskapp;
worker_processes 1;
#pid /run/nginx/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 768;
    # multi_accept on;
}

http {
    

    server {
#	    listen 80;   # There is no reason for this one, unless you want to run unencrypted for some reason.
        listen 443 ssl;
        ssl_certificate /certs/cert.pem;
        ssl_certificate_key /certs/key.pem;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;
        
        # Redirects and http request to https
        # Make sure that the port number 6443 matches from docker compose port forwarding
        error_page  497 https://$host:6443/$request_uri;
        
        # Forward to port 5000.
        location / {
                proxy_pass http://127.0.0.1:5000;
                proxy_http_version 1.1;
                proxy_buffering off;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection	"upgrade";
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Host $http_host;
                gzip off;
            }
    }

    # Basic Settings
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    # server_tokens off;

    # server_names_hash_bucket_size 64;
    # server_name_in_redirect off;

    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    # SSL Settings
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
    ssl_prefer_server_ciphers on;

    ## Logging
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    ## Gzip settings
    gzip  on;

    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    ## Virtual Host Configs
    #include /etc/nginx/conf.d/*.conf;
    #include /etc/nginx/sites-enabled/*;
}
