# Set exit on exception
set -ex
# Start NGINX - This does not work inside the container builder, but works here, which is fine.
nginx
# Start gunicorn and wrap the application.
gunicorn --bind 0.0.0.0:5000 --chdir /app wsgi:app --workers=1 --threads=15 --timeout 4000
