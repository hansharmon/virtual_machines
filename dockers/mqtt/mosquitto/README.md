# MOSQUITTO Docker
This is setup to do authentication with certificates
This is setup not forced to work with

## Create Certs
Adjust teh make_mqtt_certs.sh to place the cert whereever appropirate.
```bash
./make_mqtt_certs.sh
```

Then use those to execute pub and sub

## SUB - insecure
```bash
mosquitto_sub -t test -p 8883 --cafile /home/nanite/Projects/mqtt/certs/ca.crt --insecure
```

## PUB - insecure
```bash
mosquitto_pub -t test -p 8883 -m "hello" --insecure --cafile /home/nanite/Projects/mqtt/certs/ca.crt
```


