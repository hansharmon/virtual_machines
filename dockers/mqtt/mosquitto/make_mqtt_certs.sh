#!/usr/bin/bash
openssl genrsa -out ca.key 2048
# Generate cert from the ca key.
openssl req -new -x509 -days 3650 -key ca.key -out ca.crt  -subj "/C=US/ST=KS/L=KansasCity/O=HomeLab/OU=CertifcatAuthority/CN=192.168.0.102"

# Server Key Generation
openssl genrsa -out server.key 2048
# Server Cert Generation - CN NEEDS to match IP address.
openssl req -new -key server.key -out server.csr  -subj "/C=US/ST=KS/L=KansasCity/O=HomeLab/OU=MQTTSERVER/CN=192.168.0.102"
# Sign the server cert.
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 3650 

# Client Cert Generatation
#openssl genrsa -out client.key 2048
#openssl req -new -key client.key -out client.csr -subj "/C=US/ST=KS/L=KansasCity/O=HomeLab/OU=MQTTCLIENT/CN=RobotLabClient"
# Sign Client Cert
#openssl x509 -req -in client.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out client.crt -days 3650


chmod +r *.key *.csr *.crt
