# Gitea
[Gitea](https://about.gitea.com/) is a locally hosted git repository.

This is useful for hosting projects that you do not want up on the web or creating backups from other programs.

