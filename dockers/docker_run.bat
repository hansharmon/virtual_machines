docker run -d --restart unless-stopped -v c:/projects:/projects --publish 2201:22 --hostname nanite_build --name nanite_build hharmon/nanite_build:0.0
docker run -d --restart unless-stopped -v c:/projects:/projects --publish 2202:22 --publish 9000:9000 --hostname storm --name storm hharmon/storm:0.0
